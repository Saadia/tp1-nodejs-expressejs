const express = require("express");
const equipes = require("./equipes.json");
const joueurs = require("./joueurs.json");
const axios = require("axios");
const app = express();
const port=8083

app.use(express.json());

app.listen(port, () => {
  console.log("rest api");
});

app.get("/equipes", (req, res) => {
  // res.send("Liste des equipes")
  res.status(200).json(equipes);
});

app.get("/equipes/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const equipe = equipes.find((equipe) => equipe.id === id);
  res.status(200).json(equipe);
});

app.post("/equipes", (req, res) => {
  equipes.push(req.body);
  res.status(200).json(equipes);
});

app.put("/equipes/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const equipe = equipes.find((equipe) => equipe.id === id);
  equipe.nom = req.body.nom;
  equipe.country = req.body.country;
  res.status(200).json(equipe);
});

app.delete("/equipes/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const equipe = equipes.find((equipe) => equipe.id === id);
  equipes.splice(equipes.indexOf(equipe), 1);
  res.status(200).json(equipe);
});

// 1-Développer les opérations crud pour l’entité joueur(4 requettes)Ok
//-- get all "joueurs"
app.get("/joueurs", (req, res) => {
  res.status(200).json(joueurs);
});
//-- get "joueur" by id
app.get("/joueurs/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const joueur = joueurs.find((joueur) => joueur.id === id);
  res.status(200).json(joueur);
});
//-- add new "joueur"
app.post("/joueurs", (req, res) => {
  joueurs.push(req.body);
  res.status(200).json(joueurs);
});
//-- edit a "joueur"
app.put("/joueurs/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const joueur = joueurs.find((joueur) => joueur.id === id);
  joueur.nom = req.body.nom;
  joueur.numero = req.body.numero;
  joueur.poste = req.body.poste;
  joueur.idEquipe = req.body.idEquipe;
  res.status(200).json(joueur);
});
//-- delete a "joueur"
app.delete("/joueurs/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const joueur = joueurs.find((joueur) => joueur.id === id);
  joueurs.splice(joueurs.indexOf(joueur), 1);
  res.status(200).json(joueur);
});

//2-Développer la route permettant d’afficher les joueurs d’une équipe via son id(de l’équipe).
app.get("/equipe-joueurs/:idEquipe", (req, res) => {
  const idEquipe = parseInt(req.params.idEquipe);
  const resJoueurs = joueurs.filter((joueur) => joueur.idEquipe === idEquipe);
  res.status(200).json(resJoueurs);
});
//3-Développer la route permettant d’afficher l’équipe d’un joueur donné via son id.
app.get("/equipeDunJoueur/:idJoueur",async (req, res) => {
  const idJoueur = parseInt(req.params.idJoueur);
  const responseJoueur=await axios.get(`http://localhost:${port}/joueurs/${idJoueur}`)
  const idEquipe=responseJoueur.data.idEquipe
  const responseEquipe=await axios.get(`http://localhost:${port}/equipes/${idEquipe}`)
  const equipe=responseEquipe.data
  res.status(200).json(equipe);
});
// 4-Développer la route permettant de chercher un jour a partir de son nom
app.get("/joueursByNom/:nom", (req, res) => {
  const nom = req.params.nom;
  const resJoueurs = joueurs.filter((joueur) => joueur.nom === nom);
  res.status(200).json(resJoueurs);
});
